package imagepimp;
/*//////////////////////////////////////////////////////////////////////////
// Image Processing software: ImagePimp
// created: 2001.02.20
// copyright reserved by Dr. Chih-Cheng Hung
////////////////////////////////////////////////////////////////////////////
// edits : E. Douglas Rogers	000 19 3060
// Package(s) //////////////////////////////////////////////////////////////
// Built-In
 */
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.io.*;
import java.text.DecimalFormat;
import javax.swing.*;
// import java.net.*;
import java.applet.*;
import javax.imageio.*;
import java.net.URL;
// import javax.media.jai.*;
// import javax.media.jai.codec.*;
import java.util.*;

// Extended
// none

// Class Definition(s) /////////////////////////////////////////////////////
// ImagePimp


public class ImagePimp extends JFrame {//implements ActionListener

    // Instance Variable(s) ////////////////////////////////////////////
    // Constant(s)

    // Data Member(s)
    // Private

    // Protected
    public Container contentPane;
    public JDesktopPane desktopPane;

    // Public
    public static void main(String args[]) {
        // Generate an instance of the application
        ImagePimp app = new ImagePimp();
    }
    
    public void log (String msg){
        System.out.print(msg);
    }
    
    public int[] guiParseIntxIntInput(String msg) {
        JFrame frame = new JFrame("Window Size");
        int winSize[] = {3,3}; // default 3x3 window size
        String userInput = JOptionPane.showInputDialog(frame, "Window size like \"NumxNum\", \"3x3\", \"5x17\".");

        // todo; catch cancel input or mistyped input
        Scanner scanner = new Scanner(userInput);
        scanner.useDelimiter("x");
        if (scanner.hasNext()){
            //assumes the line has a certain structure
            try {
                winSize[0] = Integer.parseInt(scanner.next());
            } catch (NumberFormatException e) {
                log("\nUnable to process first integer, default set to 3");
                winSize[0] = 3;
            }
            try {
                winSize[1] = Integer.parseInt(scanner.next());
            } catch (NumberFormatException e) {
                log("\nUnable to process second integer, matching first integer.");
                log("error caught \n" + e);
                winSize[1] = winSize[0];
            }
            log("\nwinWidth is :" + winSize[0] + ", and winHeight is :" + winSize[1]);
        } else {
            log("Empty or invalid line. Default to 3x3");
            // winSize[0] = winSize[0] = 3;
        }
        scanner.close();

        return winSize;
    }

    // Constructor(s) //////////////////////////////////////////////////
    public ImagePimp() {
        // Generate Frame Window
        super("ImagePimp");
        initializeFrameWindow();
        
        // <editor-fold defaultstate="collapsed" desc="Testing procedures for imagePimp automation, click + sign on the left to edit.">
        /* debug test always open particular file
        File file = new File("c:/firstRun/scene00013.png");

        ImageFrame imageFrame=new ImageFrame(file);

        desktopPane.add(imageFrame);
        try{
            imageFrame.setSelected(true);
        }catch(Exception e){}
        
        // Test complete */
        
        //* debug test obtain and open all files in directory
        // http://java.dzone.com/articles/java-example-list-all-files
        //imageIO Stuff
        String directoryName = "c:/firstRun";

        File directory = new File(directoryName);

        //get all the files from a directory
        File[] fList = directory.listFiles();
        Image tImage1, tImage2, tImage3; 
        File file2 = null;
        int x = 0;
        
        for (int i = 0; i < fList.length -1; i++){

        //for (File file : fList){
            if (fList[i].isFile()){
                System.out.println(fList[i].getName());
                //ImageFrame imageFrame=new ImageFrame(fList[i]);
                //desktopPane.add(imageFrame);
                //*
                try{
                    
                    tImage1 = ImageIO.read(fList[i]);
                    tImage2 = ImageIO.read(fList[i+1]);
                    // tImage3 = edgeDetect(tImage);
                    tImage3 = bigDifference(tImage1, tImage2, 2);
                    //tImage3 = edgeDetect(tImage1);
                    
                    file2 = new File(directoryName+"/newFiles/slice"+x+".png");
                    ImageIO.write(toBufferedImage(tImage3), "png", file2);
                    x++;
                    //ImageIO.write
                } catch (Exception e) {}
                // */
            }
        }
        ImageFrame imageFrameFirst = new ImageFrame(fList[3]);
                desktopPane.add(imageFrameFirst);
        ImageFrame imageFrameSecond = new ImageFrame(fList[4]);
                desktopPane.add(imageFrameSecond);
        
        ImageFrame newImageFrame = new ImageFrame(bigDifference(imageFrameFirst.getImage(), imageFrameSecond.getImage(), 3));
        desktopPane.add(newImageFrame);
        
        newImageFrame = new ImageFrame(avgDifference(imageFrameFirst.getImage(), imageFrameSecond.getImage(), 5));
        desktopPane.add(newImageFrame);
        
        // test complete */
        
        
        
        
        
        
        //</editor-fold>
    }
    //Image to buffered image
    public static BufferedImage toBufferedImage(Image img)
    {
    if (img instanceof BufferedImage)
    {
        return (BufferedImage) img;
    }

    // Create a buffered image with transparency
    BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

    // Draw the image on to the buffered image
    Graphics2D bGr = bimage.createGraphics();
    bGr.drawImage(img, 0, 0, null);
    bGr.dispose();

    // Return the buffered image
    return bimage;
    }


    // Finalize ////////////////////////////////////////////////////////

    // Method(s) ///////////////////////////////////////////////////////
    // Private
    private void initializeFrameWindow() {
    // Get the Frame Window content pane
    contentPane = getContentPane();

    // Set Layout
    contentPane.setLayout(new BorderLayout());

    // Add a desktop pane to the Frame Window content pane
    desktopPane = new JDesktopPane();
    contentPane.add(desktopPane);

    // Window Control(s)
    // Menu Bar(s)
    JMenuBar menuBar = new JMenuBar();

        // Menu(s)
        // File Menu
        JMenu fileMenu = new JMenu("File");

            // File Menu Item(s)
            JMenuItem newFileMenuItem = new JMenuItem("New");
            JMenuItem openFileMenuItem = new JMenuItem("Open");
            JMenuItem saveFileMenuItem = new JMenuItem("Save");
            JMenuItem saveAsFileMenuItem = new JMenuItem("Save As...");
            JMenuItem closeFileMenuItem = new JMenuItem("Close");

            // Temporarily Disable Unimplemented Menu Item(s)
            newFileMenuItem.setEnabled(false);
            saveFileMenuItem.setEnabled(false);

                // Bind ActionListener(s) to Menu Item(s)
                openFileMenuItem.addActionListener(
                    new ActionListener() {
                        public void actionPerformed(ActionEvent ae) {
                            // Open a file dialog box to choose a file
                            JFileChooser fileChooser = new JFileChooser();

                            // Initialize dialog box
                            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
                            File file;

                            // Show the file dialog box
                            if (fileChooser.showOpenDialog(ImagePimp.this) != JFileChooser.CANCEL_OPTION) {
                                // If an image was selected, open that image
                                ImageFrame imageFrame=new ImageFrame(fileChooser.getSelectedFile());
                                
                                desktopPane.add(imageFrame);
                                try{
                                    imageFrame.setSelected(true);
                                }catch(Exception e){}
                            }
                        }
                    }
                );
                
                saveAsFileMenuItem.addActionListener(
                    new ActionListener() {
                        public void actionPerformed(ActionEvent ae) {
                            JFileChooser fileChooser = new JFileChooser();

                            // Initialize dialog box
                            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

                            // Show the file dialog box
                            if (fileChooser.showSaveDialog(ImagePimp.this) != JFileChooser.CANCEL_OPTION) {
                                // If an image was selected, open that image
                                ImageFrame imageFrame = (ImageFrame) desktopPane.getSelectedFrame();
                                System.out.println(imageFrame.saveImage(fileChooser.getSelectedFile()));
                            }
                        }
                    }
                );
                
                closeFileMenuItem.addActionListener(
                    new ActionListener() {
                        public void actionPerformed(ActionEvent ae) {
                            // Exit the application
                            System.exit(0);
                        }
                    }
                );

            // Add Item(s) to File Menu
            fileMenu.add(newFileMenuItem);
            fileMenu.add(openFileMenuItem);
            fileMenu.add(saveFileMenuItem);
            fileMenu.add(saveAsFileMenuItem);
            fileMenu.addSeparator();
            fileMenu.add(closeFileMenuItem);

        // Add File Menu to Menu Bar
        menuBar.add(fileMenu);

        // Transformations Menu
        JMenu transformationsMenu = new JMenu("Transformations");
            JMenu mirrorSubmenu;
            JMenu rotateSubmenu;
            JMenu histogramSubmenu;

                // Transformations Menu Item(s)
                mirrorSubmenu = new JMenu("Mirror");
                    JMenuItem mirrorSubmenuItemMH = new JMenuItem("Mirror horizontal");
                    JMenuItem mirrorSubmenuItemMV = new JMenuItem("Mirror vertical");
                rotateSubmenu = new JMenu("Rotate");
                    JMenuItem mirrorSubmenuItemRight = new JMenuItem("Rotate 90° CW");
                    JMenuItem mirrorSubmenuItemLeft = new JMenuItem("Rotate 90° CCW");
                JMenuItem grayscaleTransformationsMenuItem = new JMenuItem("Grayscale");
                histogramSubmenu = new JMenu("Histogram");
                    JMenuItem histogramMenuItem = new JMenuItem("Histogram");
                    JMenuItem histogramEqMenuItem = new JMenuItem("HistogramEq");
                    JMenuItem histogramEqColorMenuItem = new JMenuItem("HistogramEqColor");
                JMenuItem customTransformationsMenuItem = new JMenuItem("Custom...");
                JMenuItem edgeDetectMenuItem = new JMenuItem("Edge Detect");				
                JMenuItem segmentationMenuItem = new JMenuItem("Segmentation");
                segmentationMenuItem.setEnabled(false);
                customTransformationsMenuItem.setEnabled(false);

                    // Bind ActionListener(s) to Menu Item(s)
                    mirrorSubmenuItemMV.addActionListener(
                        new ActionListener() {
                            public void actionPerformed(ActionEvent ae) {
                                // Call mirrorVertical method
                                ImageFrame imageFrame = (ImageFrame) desktopPane.getSelectedFrame();
                                ImageFrame newImageFrame = new ImageFrame(mirrorVertical(imageFrame.getImage()));
                                desktopPane.add(newImageFrame);
                                newImageFrame.toFront();
                                try{newImageFrame.setSelected(true);}catch(Exception e){}
                            }
                        }
                    );
                    mirrorSubmenuItemMH.addActionListener(
                        new ActionListener() {
                            public void actionPerformed(ActionEvent ae) {
                                // Call mirrorHorizontal method
                                ImageFrame imageFrame = (ImageFrame) desktopPane.getSelectedFrame();
                                ImageFrame newImageFrame = new ImageFrame(mirrorHorizontal(imageFrame.getImage()));
                                desktopPane.add(newImageFrame);
                                newImageFrame.toFront();
                                try{newImageFrame.setSelected(true);}catch(Exception e){}
                            }
                        }
                    );
                    mirrorSubmenuItemLeft.addActionListener(
                        new ActionListener() {
                            public void actionPerformed(ActionEvent ae) {
                                // Call rotateLeft method
                                ImageFrame imageFrame = (ImageFrame) desktopPane.getSelectedFrame();
                                ImageFrame newImageFrame = new ImageFrame(rotateLeft(imageFrame.getImage()));
                                desktopPane.add(newImageFrame);
                                newImageFrame.toFront();
                                try{newImageFrame.setSelected(true);}catch(Exception e){}
                            }
                        }
                    );
                    mirrorSubmenuItemRight.addActionListener(
                        new ActionListener() {
                            public void actionPerformed(ActionEvent ae) {
                                // Call rotateRight method
                                ImageFrame imageFrame = (ImageFrame) desktopPane.getSelectedFrame();
                                ImageFrame newImageFrame = new ImageFrame(rotateRight(imageFrame.getImage()));
                                desktopPane.add(newImageFrame);
                                newImageFrame.toFront();
                                try{newImageFrame.setSelected(true);}catch(Exception e){}
                            }
                        }
                    );
                    grayscaleTransformationsMenuItem.addActionListener(
                        new ActionListener() {
                            public void actionPerformed(ActionEvent ae) {
                                // Call contrast enhancement method: colorToGrayscale
                                ImageFrame imageFrame = (ImageFrame) desktopPane.getSelectedFrame();
                                ImageFrame newImageFrame = new ImageFrame(colorToGrayscale(imageFrame.getImage()));
                                desktopPane.add(newImageFrame);
                                newImageFrame.toFront();
                                try{newImageFrame.setSelected(true);}catch(Exception e){}
                            }
                        }
                    );
                    histogramMenuItem.addActionListener(
                        new ActionListener() {
                            public void actionPerformed(ActionEvent ae) {
                                // Call histogram method
                                ImageFrame imageFrame = (ImageFrame) desktopPane.getSelectedFrame();
                                ImageFrame newImageFrame = new ImageFrame(histogram(imageFrame.getImage()));
                                desktopPane.add(newImageFrame);
                                newImageFrame.toFront();
                                try{newImageFrame.setSelected(true);}catch(Exception e){}
                            }
                        }
                    );
                    histogramEqMenuItem.addActionListener(
                        new ActionListener() {
                            public void actionPerformed(ActionEvent ae) {
                                // Call histogram method
                                ImageFrame imageFrame = (ImageFrame) desktopPane.getSelectedFrame();
                                ImageFrame newImageFrame = new ImageFrame(histogramEq(imageFrame.getImage()));
                                desktopPane.add(newImageFrame);
                                newImageFrame.toFront();
                                try{newImageFrame.setSelected(true);}catch(Exception e){}
                            }
                        }
                    );
                    histogramEqColorMenuItem.addActionListener(
                        new ActionListener() {
                            public void actionPerformed(ActionEvent ae) {
                                // Call histogram method
                                ImageFrame imageFrame = (ImageFrame) desktopPane.getSelectedFrame();
                                ImageFrame newImageFrame = new ImageFrame(histogramEqColor(imageFrame.getImage()));
                                desktopPane.add(newImageFrame);
                                newImageFrame.toFront();
                                try{newImageFrame.setSelected(true);}catch(Exception e){}
                            }
                        }
                    );
                    edgeDetectMenuItem.addActionListener(
                        new ActionListener() {
                            public void actionPerformed(ActionEvent ae) {
                                ImageFrame imageFrame = (ImageFrame) desktopPane.getSelectedFrame();
                                ImageFrame newImageFrame = new ImageFrame(edgeDetect(imageFrame.getImage()));
                                desktopPane.add(newImageFrame);
                                newImageFrame.toFront();
                                try{newImageFrame.setSelected(true);}catch(Exception e){}
                            }
                        }
                    );

                    // mirrorSubmenuItemLeft

                    // Add Item(s) to Transformations Menu
                        mirrorSubmenu.add(mirrorSubmenuItemMV);
                        mirrorSubmenu.add(mirrorSubmenuItemMH);
                    transformationsMenu.add(mirrorSubmenu);
                        rotateSubmenu.add(mirrorSubmenuItemLeft);
                        rotateSubmenu.add(mirrorSubmenuItemRight);
                    transformationsMenu.add(rotateSubmenu);
                    transformationsMenu.add(grayscaleTransformationsMenuItem);
                        histogramSubmenu.add(histogramMenuItem);
                        histogramSubmenu.add(histogramEqMenuItem);
                        histogramSubmenu.add(histogramEqColorMenuItem);
                    transformationsMenu.add(histogramSubmenu);
                    transformationsMenu.add(segmentationMenuItem);
                    transformationsMenu.add(edgeDetectMenuItem);
                    transformationsMenu.addSeparator();
                    transformationsMenu.add(customTransformationsMenuItem);

        // Add Transformations Menu to Menu Bar
        menuBar.add(transformationsMenu);

        // Filters Menu
        JMenu filtersMenu = new JMenu("Filter");

            // Filters Menu Item(s)
            JMenuItem contrastEnhancementFiltersMenuItem = new JMenuItem("Contrast Enhancement");
            JMenuItem averageImageMenuItem = new JMenuItem("Average Image");
            JMenuItem medianFilterMenuItem = new JMenuItem("Median Filter (RGB separate)");
            JMenuItem medianFilterSquaredDiffMenuItem = new JMenuItem("Median Filter (sq. diff.)");
            JMenuItem tomitaFilterMenuItem = new JMenuItem("Tomita Filter");
            JMenuItem iterativeThresholdMenuItem = new JMenuItem("Iterative Threshold");
            JMenuItem optimalThresholdMenuItem = new JMenuItem("Optimal Threshold");
            JMenuItem noiseFilterMenuItem = new JMenuItem("Random Noise");
            JMenuItem psnrCheckMenuItem = new JMenuItem("PSNR value");
            JMenuItem customFiltersMenuItem = new JMenuItem("Custom...");
            //;

            // Temporarily Disable Unimplemented Menu Item(s)
            contrastEnhancementFiltersMenuItem.setEnabled(false);
            customFiltersMenuItem.setEnabled(false);

            // Bind ActionListener(s) to Menu Item(s)
            averageImageMenuItem.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent ae) {
                        int winSize[] = guiParseIntxIntInput("Window size like \"NumxNum\", \"3x3\", \"5x17\".");	    	
                        ImageFrame imageFrame = (ImageFrame) desktopPane.getSelectedFrame();
                        ImageFrame newImageFrame = new ImageFrame(averageImage(imageFrame.getImage(), winSize));
                        desktopPane.add(newImageFrame);
                        newImageFrame.toFront();
                        try{newImageFrame.setSelected(true);}catch(Exception e){}
                    }
                }
            );
            medianFilterMenuItem.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent ae) {
                        // debugging: temp default window size 3x3
                        int winSize[] = {3,3}; // guiParseIntxIntInput("Window size like \"NumxNum\", \"3x3\", \"5x17\".");
                        ImageFrame imageFrame = (ImageFrame) desktopPane.getSelectedFrame();
                        ImageFrame newImageFrame = new ImageFrame(medianFilterRGBseparate(imageFrame.getImage()));
                        desktopPane.add(newImageFrame);
                        newImageFrame.toFront();
                        try{newImageFrame.setSelected(true);}catch(Exception e){}
                    }
                }
            );

            // Bind ActionListener(s) to Menu Item(s)
            medianFilterSquaredDiffMenuItem.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent ae) {
                        // debugging: temp default window size 3x3
                        int winSize[] = {3,3}; // guiParseIntxIntInput("Window size like \"NumxNum\", \"3x3\", \"5x17\".");
                        ImageFrame imageFrame = (ImageFrame) desktopPane.getSelectedFrame();
                        ImageFrame newImageFrame = new ImageFrame(medianFilterSumSquaredDifferences(imageFrame.getImage()));
                        desktopPane.add(newImageFrame);
                        newImageFrame.toFront();
                        try{newImageFrame.setSelected(true);}catch(Exception e){}
                    }
                }
            );

            // Bind ActionListener(s) to Menu Item(s)
            tomitaFilterMenuItem.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent ae) {
                        // debugging: temp default window size 3x3
                        int winSize[] = {3,3}; // guiParseIntxIntInput("Window size like \"NumxNum\", \"3x3\", \"5x17\".");
                        ImageFrame imageFrame = (ImageFrame) desktopPane.getSelectedFrame();
                        ImageFrame newImageFrame = new ImageFrame(tomitaFilter(imageFrame.getImage()));
                        desktopPane.add(newImageFrame);
                        newImageFrame.toFront();
                        try{newImageFrame.setSelected(true);}catch(Exception e){}
                    }
                }
            );
            
            // Bind ActionListener(s) to Menu Item(s)
            iterativeThresholdMenuItem.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent ae) {
                        ImageFrame imageFrame = (ImageFrame) desktopPane.getSelectedFrame();
                        ImageFrame newImageFrame = new ImageFrame(iterativeThreshold(imageFrame.getImage()));
                        desktopPane.add(newImageFrame);
                        newImageFrame.toFront();
                        try{newImageFrame.setSelected(true);}catch(Exception e){}
                    }
                }
            );
            
            // Bind ActionListener(s) to Menu Item(s)
            optimalThresholdMenuItem.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent ae) {
                        ImageFrame imageFrame = (ImageFrame) desktopPane.getSelectedFrame();
                        ImageFrame newImageFrame = new ImageFrame(optimalThreshold(imageFrame.getImage()));
                        desktopPane.add(newImageFrame);
                        newImageFrame.toFront();
                        try{newImageFrame.setSelected(true);}catch(Exception e){}
                    }
                }
            );

            // Bind ActionListener(s) to Menu Item(s)
            noiseFilterMenuItem.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent ae) {
                        // temp default noise 
                        ImageFrame imageFrame = (ImageFrame) desktopPane.getSelectedFrame();
                        ImageFrame newImageFrame = new ImageFrame(addNoise(imageFrame.getImage()));
                        desktopPane.add(newImageFrame);
                        newImageFrame.toFront();
                        try{newImageFrame.setSelected(true);}catch(Exception e){}
                    }
                }
            );

            // Bind ActionListener(s) to Menu Item(s)
            psnrCheckMenuItem.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent ae) {
                        // temp default noise 
                        ImageFrame imageFrame = (ImageFrame) desktopPane.getSelectedFrame();
                        System.out.println("PSNR = " + compareNoise(imageFrame.getImage()) + " dB");
                    }
                }
            );

            // Add Item(s) to Filters Menu
            filtersMenu.add(contrastEnhancementFiltersMenuItem);
            filtersMenu.add(averageImageMenuItem);
            filtersMenu.add(medianFilterMenuItem);
            filtersMenu.add(medianFilterSquaredDiffMenuItem);
            filtersMenu.add(tomitaFilterMenuItem);
            filtersMenu.add(iterativeThresholdMenuItem);
            filtersMenu.add(optimalThresholdMenuItem);
            filtersMenu.addSeparator();
            filtersMenu.add(noiseFilterMenuItem);
            filtersMenu.add(psnrCheckMenuItem);
            filtersMenu.add(customFiltersMenuItem);

        // Add Filters Menu to Menu Bar
        menuBar.add(filtersMenu);

        // Add Menu Bar to Frame Window
        setJMenuBar(menuBar);

        // Register the window listener to terminate the application
        addWindowListener(
            new WindowAdapter() {
                // Override window closing with application termination
                public void windowClosing(WindowEvent we)
                {
                    // Terminate application
                    System.exit(0);
                }
            }
        );

        // Display the Frame Window
        setSize(700, 600);
        setVisible(true);
    }

    // Protected
    protected Image mirrorHorizontal(Image imageIn) {
        // Declare local storage
        Dimension imageInDimension = getImageDimension(imageIn);
        int TRGB[][][] = pixelsArrayToTRGBArray(imageToPixelsArray(imageIn), imageInDimension);

        int imgHeight = (int) imageInDimension.getHeight();
        int tmp;
        // Loop through image, transposing horizontal values
        // (add menu item and routine to transpose vertical dimension)
        for (int column = 0; column < imageInDimension.getWidth(); column++)
            for (int row = 0; row < imgHeight/2; row++)
                for (int i = 0; i < 4; i++) {
                    tmp = TRGB[i][column][row];
                    TRGB[i][column][row] = TRGB[i][column][(int) (imgHeight -1 - row)];
                    TRGB[i][column][(int) (imgHeight -1 - row)] = tmp;
                }
        // Convert TRGB array back to image for return
        return pixelsArrayToImage(TRGBArrayToPixelsArray(TRGB, imageInDimension), imageInDimension);
    }
    
    protected Image mirrorVertical(Image imageIn) {
        // Declare local storage
        Dimension imageInDimension = getImageDimension(imageIn);
        int TRGB[][][] = pixelsArrayToTRGBArray(imageToPixelsArray(imageIn), imageInDimension);

        int imgWidth = (int) imageInDimension.getWidth();
        int tmp;
        // Loop through image, transposing horizontal values
        // (add menu item and routine to transpose vertical dimension)
        for (int row = 0; row < (int) imageInDimension.getHeight(); row++)
            for (int column = 0; column < imgWidth/2; column++)
                for (int i = 0; i < 4; i++) {
                    tmp = TRGB[i][column][row];
                    TRGB[i][column][row] = TRGB[i][(int) (imgWidth -1 - column)][row];
                    TRGB[i][(int) (imgWidth -1 - column)][row] = tmp;
                }
        // Convert TRGB array back to image for return
        return pixelsArrayToImage(TRGBArrayToPixelsArray(TRGB, imageInDimension), imageInDimension);
    }

    protected Image rotateRight(Image imageIn) {
        // Declare local storage
        Dimension imageInDimension = getImageDimension(imageIn);
        Dimension imageOutDimension = getImageDimension(imageIn);
        imageOutDimension.setSize(imageInDimension.getHeight(), imageInDimension.getWidth());
        
        int TRGB[][][] = pixelsArrayToTRGBArray(imageToPixelsArray(imageIn), imageInDimension);
        int altTRGB[][][] = new int [4][(int) imageInDimension.getHeight()][(int) imageInDimension.getWidth()];

        int imgHeight = (int)imageInDimension.getHeight();
        int imgWidth = (int)imageInDimension.getWidth();

        for (int row = 0; row < imgHeight; row++)
            for (int column = 0; column < imgWidth; column++)
                for (int i = 0; i < 4; i++)
                    altTRGB[i][imgHeight -1 - row][column] = TRGB[i][column][row];	

        return pixelsArrayToImage(TRGBArrayToPixelsArray(altTRGB, imageOutDimension), imageOutDimension);
    }

    protected Image rotateLeft(Image imageIn) {
        // Declare local storage
        Dimension imageInDimension = getImageDimension(imageIn);
        Dimension imageOutDimension = getImageDimension(imageIn);
        imageOutDimension.setSize(imageInDimension.getHeight(), imageInDimension.getWidth());

        int TRGB[][][] = pixelsArrayToTRGBArray(imageToPixelsArray(imageIn), imageInDimension);
        int altTRGB[][][] = new int [4][(int) imageInDimension.getHeight()][(int) imageInDimension.getWidth()];

        int imgHeight = (int)imageInDimension.getHeight();
        int imgWidth = (int)imageInDimension.getWidth();

        for (int row = 0; row < imgHeight; row++)
            for (int column = 0; column < imgWidth; column++)
                for (int i = 0; i < 4; i++)
                    altTRGB[i][row][imgWidth -1 - column] = TRGB[i][column][row];	

        return pixelsArrayToImage(TRGBArrayToPixelsArray(altTRGB, imageOutDimension), imageOutDimension);
    }

    protected Image colorToGrayscale(Image imageIn) {
        // Declare local storage
        Dimension imageInDimension = getImageDimension(imageIn);
        int TRGB[][][] = pixelsArrayToTRGBArray(imageToPixelsArray(imageIn), imageInDimension);

        // Loop through image, averaging color(RGB) values
        for (int row = 0; row < imageInDimension.getHeight(); row++)
            for (int column = 0; column < imageInDimension.getWidth(); column++)
            {
                // Average values and store back into TRGB array
                int average = (TRGB[1][column][row] + TRGB[2][column][row] + TRGB[3][column][row]) / 3;
                /*
                int blow = 50;
                int bhi = 200;
                if (average < blow) {
                    average = 0;
                } else if (average > bhi) {
                    average = 255;
                } else
                    average = 255 * (average - blow) / (bhi - blow);
                */
                TRGB[1][column][row] = average;
                TRGB[2][column][row] = average;
                TRGB[3][column][row] = average;
            }
        // Convert TRGB array back to image for return
        return pixelsArrayToImage(TRGBArrayToPixelsArray(TRGB, imageInDimension), imageInDimension);
    }

    protected Image histogram(Image imageIn) {
        // Declare local storage
        Dimension imageInDimension = getImageDimension(imageIn);
        int TRGB[][][] = pixelsArrayToTRGBArray(imageToPixelsArray(imageIn), imageInDimension);

        // get histogramInt; (average RGB values)
        int histogramInt[] = extractHistogram(imageIn);
        int histogramCumulative[] = new int [256];
        int altTRGBheight = 256; //(int)imageInDimension.getHeight()/2;
        int altTRGB[][][] = new int [4][histogramInt.length][altTRGBheight];
        int countHeight;
        int maxHistogram = -1;
        double HISTOGRAM_SCALE_FACTOR = 1;
        Dimension imageOutDimension = getImageDimension(imageIn);
        imageOutDimension.setSize(histogramInt.length, altTRGBheight);


        // get maxHistogram value
        for (int i = 0; i < histogramInt.length; i++) {
            if (histogramInt[i] > maxHistogram) maxHistogram = histogramInt[i];
        }

        // store cumulative histogram
        histogramCumulative[0] += histogramInt[0];
        for (int column = 1; column < histogramInt.length; column++) {
            histogramCumulative[column] = histogramCumulative[column -1] + histogramInt[column];
        }

        // http://stackoverflow.com/questions/863507/whats-the-best-way-to-set-all-values-of-a-three-dimensional-array-to-zero-in-ja
        // set transparency off, background white
        for (int column = 0; column < histogramInt.length; column++)
            for (int row = 0; row < altTRGBheight; row++)
                for (int trans = 0; trans < 4; trans++)
                    altTRGB[trans][column][row] = 255;

        // alter histogram image to show count totals
        int previousCountHeight = 0;
        for (int column = 0; column < histogramInt.length; column++) {
            // display image histogram in red
            // determine relative height of histogram count , fill pixels to that height
            countHeight = (int) (histogramInt[column] / (double) maxHistogram * (altTRGBheight-1) * HISTOGRAM_SCALE_FACTOR);
            for(int row = altTRGBheight -1 ; row > altTRGBheight -1 - countHeight  ; row--) {
                altTRGB[1][column][row] = 220;
                altTRGB[2][column][row] = 20;
                altTRGB[3][column][row] = 60;
            }

            // include cumulative histogram in image.
            countHeight = (int) ((histogramCumulative[column] / (double) (TRGB[0].length * TRGB[0][0].length)) * (altTRGBheight-1) * HISTOGRAM_SCALE_FACTOR);

            // added loop to draw line from bottom of previous histogram mark
            for (int i = previousCountHeight; i <= countHeight; i++) {
                // System.out.print((altTRGBheight -1 + i) + " row ");
                for (int j = 1; j < 4; j++)
                    altTRGB[j][column][altTRGBheight -1 - i] = 0;
            }
            previousCountHeight = countHeight;

        }

        return pixelsArrayToImage(TRGBArrayToPixelsArray(altTRGB, imageOutDimension), imageOutDimension);
    }

    protected int[] extractHistogram(Image imageIn) {
        // Declare local storage
        Dimension imageInDimension = getImageDimension(imageIn);
        int TRGB[][][] = pixelsArrayToTRGBArray(imageToPixelsArray(imageIn), imageInDimension);
        int histogramInt[] = new int [256];

        // Loop through image, store average RGB values
        for (int row = 0; row < imageInDimension.getHeight(); row++)
            for (int column = 0; column < imageInDimension.getWidth(); column++) {
                // Average values and store pixel counts back into count array
                int average = (TRGB[1][column][row] + TRGB[2][column][row] + TRGB[3][column][row]) / 3;
                histogramInt[average]++;
            }
        return histogramInt;
    }

    protected Image histogramEq(Image imageIn) {
        // Declare local storage
        Dimension imageInDimension = getImageDimension(imageIn);
        int TRGB[][][] = pixelsArrayToTRGBArray(imageToPixelsArray(imageIn), imageInDimension);
        int histogramMap[] =  new int [256];
        int histogramInt[] = extractHistogram(imageIn);
        int height = (int) imageInDimension.getHeight();
        int width = (int) imageInDimension.getWidth();
        int totalPixels = height * width;
        int sum = 0;
        int average;

        // create histogram map
        for (int i = 0; i < histogramInt.length; i++) {
            sum += histogramInt[i];
            histogramMap[i] = (int) ((1.0 * sum / totalPixels) * 255);
        }

        // Loop through image, averaging color(RGB) values
        for (int row = 0; row < imageInDimension.getHeight(); row++)
            for (int column = 0; column < imageInDimension.getWidth(); column++) {
                // Average values into TRGB array
                average = (TRGB[1][column][row] + TRGB[2][column][row] + TRGB[3][column][row]) / 3;
                TRGB[1][column][row] = histogramMap[average];
                TRGB[2][column][row] = histogramMap[average];
                TRGB[3][column][row] = histogramMap[average];
            }
        // Convert TRGB array back to image for return
        return pixelsArrayToImage(TRGBArrayToPixelsArray(TRGB, imageInDimension), imageInDimension);
    }

    protected Image histogramEqColor(Image imageIn) {
        // Declare local storage
        Dimension imageInDimension = getImageDimension(imageIn);
        int TRGB[][][] = pixelsArrayToTRGBArray(imageToPixelsArray(imageIn), imageInDimension);
        int histogramMapRed[] =  new int [256];
        int histogramMapGreen[] =  new int [256];
        int histogramMapBlue[] =  new int [256];
        int histogramRed[] =  new int [256];
        int histogramGreen[] =  new int [256];
        int histogramBlue[] =  new int [256];
        int height = (int) imageInDimension.getHeight();
        int width = (int) imageInDimension.getWidth();
        int totalPixels = height * width;
        int redFraction = 0;
        int greenFraction = 0;
        int blueFraction = 0;

        // Loop through image, store quantities of individual RGB values
        for (int row = 0; row < imageInDimension.getHeight(); row++)
            for (int column = 0; column < imageInDimension.getWidth(); column++) {
                // store pixel counts back into count array
                histogramRed[TRGB[1][column][row]]++;
                histogramGreen[TRGB[2][column][row]]++;
                histogramBlue[TRGB[3][column][row]]++;
            }

        // create individual histogram color maps
        for (int i = 0; i < histogramRed.length; i++) {
            redFraction += histogramRed[i];
            histogramMapRed[i] = (int) ((1.0 * redFraction / totalPixels) * 255);
            greenFraction += histogramGreen[i];
            histogramMapGreen[i] = (int) ((1.0 * greenFraction / totalPixels) * 255);
            blueFraction += histogramBlue[i];
            histogramMapBlue[i] = (int) ((1.0 * blueFraction / totalPixels) * 255);
        }

        // Loop through image, averaging color(RGB) values
        for (int row = 0; row < imageInDimension.getHeight(); row++)
            for (int column = 0; column < imageInDimension.getWidth(); column++)
            {
                // Average values into TRGB array
                TRGB[1][column][row] = histogramMapRed[TRGB[1][column][row]];
                TRGB[2][column][row] = histogramMapGreen[TRGB[2][column][row]];
                TRGB[3][column][row] = histogramMapBlue[TRGB[3][column][row]];
            }
        // Convert TRGB array back to image for return
        return pixelsArrayToImage(TRGBArrayToPixelsArray(TRGB, imageInDimension), imageInDimension);
    }

    protected Image edgeDetect(Image imageIn) {
        Dimension imageInDimension = getImageDimension(imageIn);
        int TRGB[][][] = pixelsArrayToTRGBArray(imageToPixelsArray(imageIn), imageInDimension);
        int altTRGB[][][] = pixelsArrayToTRGBArray(imageToPixelsArray(imageIn), imageInDimension);
        for (int colorComponent = 1; colorComponent < 4; colorComponent++)
            for (int x = 0; x < imageInDimension.getHeight(); x++)
                for (int y = 0; y < imageInDimension.getWidth(); y++) {
                    int fx = 0;
                    int fy = 0;
                    if((x-1>0)&&(x+1<imageInDimension.getHeight()))
                        fx=TRGB[colorComponent][y][x-1]-TRGB[colorComponent][y][x+1];
                    if((y-1>0)&&(y+1<imageInDimension.getWidth()))
                        fy=TRGB[colorComponent][y-1][x]-TRGB[colorComponent][y+1][x];
                    altTRGB[colorComponent][y][x]=(int)Math.sqrt(Math.pow((fy),2)+Math.pow((fx),2));
                }
        return pixelsArrayToImage(TRGBArrayToPixelsArray(altTRGB, imageInDimension), imageInDimension);
    }

    protected double entropy(Image imageIn) {
        // Declare local storage
        Dimension imageInDimension = getImageDimension(imageIn);
        int TRGB[][][] = pixelsArrayToTRGBArray(imageToPixelsArray(imageIn), imageInDimension);
        double totalPixels = imageInDimension.getHeight() * imageInDimension.getWidth();
        int greyscale[] = new int [256];
        double entropy = 0;

        // Loop through image, averaging color(RGB) values
        for (int row = 0; row < imageInDimension.getHeight(); row++)
            for (int column = 0; column < imageInDimension.getWidth(); column++)
                greyscale[(TRGB[1][column][row] + TRGB[2][column][row] + TRGB[3][column][row]) / 3]++;

        // run entropy calculation for each non-0 value of greyscale array
        for (int i = 0; i < greyscale.length; i++)
            if (greyscale[i]>0)
                entropy -= (greyscale[i]/totalPixels) * Math.log(greyscale[i]/totalPixels);

        return entropy;
    }

    protected Image averageImage(Image imageIn, int[] winSize) {
        Dimension imageInDimension = getImageDimension(imageIn);
        int TRGB[][][] = pixelsArrayToTRGBArray(imageToPixelsArray(imageIn), imageInDimension);
        // copy image
        int altTRGB[][][] = pixelsArrayToTRGBArray(imageToPixelsArray(imageIn), imageInDimension);
        int halfWindow[] = {winSize[0]/2, winSize[1]/2};
        int avg[] = new int [3];

        // todo: reduce iterations (store column calculations for next iteration)
        for (int row = 0 + halfWindow[1]; row < imageInDimension.getHeight() - halfWindow[1]; row++){
            for (int column = 0 + halfWindow[0]; column < imageInDimension.getWidth() - halfWindow[0]; column++) {
                avg[0] = avg[1] = avg [2] = 0;  // reset store for average color value
                for (int color = 0; color < 3; color++)
                    for (int x = 0 - halfWindow[0]; x < 0 - halfWindow[0] + winSize[0]; x++)
                        for (int y = 0 - halfWindow[1]; y < 0 - halfWindow[1] + winSize[1]; y++)
                            avg[color] += TRGB[color + 1][column + x][row + y];

                // assign average of colors into new array
                for (int color = 0; color < 3; color++)
                    altTRGB[color + 1][column][row] = (int) (0.0 + avg[color])/(winSize[0] * winSize[1]);
            }
        }

        // entropy calculation
        final DecimalFormat df = new DecimalFormat();
        df.setMinimumFractionDigits(5);
        log("\nEntropy:" + df.format(entropy(imageIn)) + "\n");

        return pixelsArrayToImage(TRGBArrayToPixelsArray(altTRGB, imageInDimension), imageInDimension);
    }
    
    protected int[] medianPixelValues(int[][][] TRGB, int x, int y, int xWinStart, int yWinStart){
        int[] rgb = new int [3];
        int medianLocator[] = new int[9];
        
        for (int colorComponent = 1; colorComponent < 4; colorComponent++){
            int i = 0;
            for (int xWin = xWinStart; xWin < xWinStart + 3; xWin++) {
                for (int yWin = yWinStart; yWin < yWinStart + 3; yWin++){
                    medianLocator[i] = TRGB[colorComponent][y + yWin][x + xWin];
                    i++;
                }
            }
            Arrays.sort(medianLocator);
            rgb[colorComponent - 1] = medianLocator[4];
        }
        
        return rgb;
    }
    
    protected Image medianFilterRGBseparate(Image imageIn) {
        Dimension imageInDimension = getImageDimension(imageIn);
        int TRGB[][][] = pixelsArrayToTRGBArray(imageToPixelsArray(imageIn), imageInDimension);
        int altTRGB[][][] = pixelsArrayToTRGBArray(imageToPixelsArray(imageIn), imageInDimension);
        
        for (int x = 1; x < imageInDimension.getHeight() - 1; x++) {
            for (int y = 1; y < imageInDimension.getWidth() -1; y++) {

                int rgb[] = medianPixelValues(TRGB, x, y, -1, -1);
                for (int colorComponent = 1; colorComponent < 4; colorComponent++){
                    altTRGB[colorComponent][y][x] = rgb[colorComponent - 1];
                }

            }
        }
        
        return pixelsArrayToImage(TRGBArrayToPixelsArray(altTRGB, imageInDimension), imageInDimension);
    }

    protected Image medianFilterSumSquaredDifferences(Image imageIn) {
        /*
        The median filter for color images operates differently from the grayscale 
        median filter. Since each pixel in an RGB color image is composed of three 
        components (red, green, and blue), it is not useful to rank the pixels in 
        the neighborhood according to brightness. Instead, the color median filter 
        works by comparing each pixel's color to that of every other pixel in the 
        neighborhood. The pixel whose red, green, and blue components have the 
        smallest sum of squared differences from the color coordinates of its 
        neighbors is then chosen to replace the central pixel of the neighborhood.
        
        add these filters...
        sigma filter
        LoG 
        Tomita
        Nagao
        gaussian noise
        */
        Dimension imageInDimension = getImageDimension(imageIn);
        int TRGB[][][] = pixelsArrayToTRGBArray(imageToPixelsArray(imageIn), imageInDimension);
        int altTRGB[][][] = pixelsArrayToTRGBArray(imageToPixelsArray(imageIn), imageInDimension);
        
        for (int x = 1; x < imageInDimension.getHeight() - 1; x++) {
            for (int y = 1; y < imageInDimension.getWidth() -1; y++) {

                int rgb[] = medianPixelValues(TRGB, x, y, -1, -1);
                for (int colorComponent = 1; colorComponent < 4; colorComponent++){
                    altTRGB[colorComponent][y][x] = rgb[colorComponent - 1];
                }

            }
        }
        
        return pixelsArrayToImage(TRGBArrayToPixelsArray(altTRGB, imageInDimension), imageInDimension);
    }

    protected Image tomitaFilter(Image imageIn) {
        /*
        Variance is the average of the squared differences from the Mean.
        
        */
        Dimension imageInDimension = getImageDimension(imageIn);
        int TRGB[][][] = pixelsArrayToTRGBArray(imageToPixelsArray(imageIn), imageInDimension);
        int altTRGB[][][] = pixelsArrayToTRGBArray(imageToPixelsArray(imageIn), imageInDimension);
        int tomitaStart[] = {-2,-2,0,-2,-2,0,0,0,-1,-1};
        double localMean = 0; 
        double accumulator;
        int xTot, yTot;
        
        // pixel by pixel
        for (int x = 2; x < imageInDimension.getHeight() - 2; x++) {
            for (int y = 2; y < imageInDimension.getWidth() - 2; y++) {
                
                // initialize localVariance values
                
                double localVariance[] = {0, 0, 0, 0, 0};
                for (int j = 0; j < 5; j++){
                    int i = 0;
                    // identify local mean of each of 5 regions
                    for (int xWin = tomitaStart[j * 2]; xWin < tomitaStart[j * 2] + 3; xWin++) {
                        for (int yWin = tomitaStart[(j * 2) + 1]; yWin < tomitaStart[(j * 2) + 1] + 3; yWin++){
                            for (int colorComponent = 1; colorComponent < 4; colorComponent++){
                                xTot = x + xWin;
                                yTot = y + yWin;
                                localMean += TRGB[colorComponent][yTot][xTot];
                                i++;
                            }
                        }
                    }
                    localMean = localMean / i;
                    
                    // identify the variances of the 5 regions
                    for (int xWin = -1; xWin < 2; xWin++) {
                        for (int yWin = -1; yWin < 2; yWin++){
                            accumulator = 0;
                            for (int colorComponent = 1; colorComponent < 4; colorComponent++){
                                xTot = x + xWin;
                                yTot = y + yWin;
                                accumulator += TRGB[colorComponent][yTot][xTot];
                                i++;
                            }
                            localVariance[j] = accumulator / localMean;
                        }
                    }
                }
                
                // identify the region with lowest variance
                int minRegion = 0;
                double minVariance = localVariance[minRegion];
                for (int i = 1; i < localVariance.length; i++){
                    if (localVariance[i] < minVariance){
                        minVariance = localVariance[i];
                        minRegion = i;
                    }
                }
                
                // replace altTRGB pixel with mean from region with lowest variance
                int rgb[] = medianPixelValues(TRGB, x, y, tomitaStart[minRegion * 2], tomitaStart[(minRegion * 2) + 1]);
                for (int colorComponent = 1; colorComponent < 4; colorComponent++){
                    altTRGB[colorComponent][y][x] = rgb[colorComponent - 1];
                }

            }
        }
        
        return pixelsArrayToImage(TRGBArrayToPixelsArray(altTRGB, imageInDimension), imageInDimension);
    }

    protected Image addNoise(Image imageIn) {
        // declare storage
        Dimension imageInDimension = getImageDimension(imageIn);
        int TRGB[][][] = pixelsArrayToTRGBArray(imageToPixelsArray(imageIn), imageInDimension);
        // hard coded 10% noise
        double noise = 0.1;
        
        // new random generator
        Random randomGen = new Random();

        for (int row = 0; row < imageInDimension.getHeight(); row++)
            for (int column = 0; column < imageInDimension.getWidth(); column++) {
                // introduce random noise to image
                if (randomGen.nextDouble() < noise){
                    for (int color = 1; color <= 3; color++) {
                        TRGB[color][column][row] = randomGen.nextInt(256);
                    }
                }
            }
        return pixelsArrayToImage(TRGBArrayToPixelsArray(TRGB, imageInDimension), imageInDimension);
    }
    
    protected double meanGrey(int[] histogram, double bottom, double top) {
        double result = 0;
        int count = 0;
        // log(bottom + " = bottom : " + top + " = top\n");
        for (int i = (int) bottom; i <= (int) top; i++){
            result += i * histogram[i];
            count += histogram[i];
        }
        return result / count;
    }
    
    protected double iterThresholdSelect(int[] histogram) {
        // takes a histogram map and a meanGrey level, returns new threshold
        double tSubi, tSubi1, tSubb, tSubw;
        
        tSubi = meanGrey(histogram, 0, 255);
        
        do {
            tSubi1 = tSubi;
            tSubb = meanGrey(histogram, 0, (int) tSubi1 - 1);
            tSubw = meanGrey(histogram, (int) tSubi1, histogram.length - 1);
            tSubi = (tSubb + tSubw)/2.0; 
             log(tSubi + " = (" + tSubb  + " + " + tSubw + ")/2.0 : tSubi = (tSubb + tSubw)/2.0;  tSubi1 = " + tSubi1 + "\n");           
        } while (tSubi != tSubi1);
        log("result iterative threshold: " + tSubi + "\n");
        return tSubi;
    }
    
    protected Image iterativeThreshold (Image imageIn){
        // declare storage
        Dimension imageInDimension = getImageDimension(imageIn);
        // int TRGB[][][] = pixelsArrayToTRGBArray(imageToPixelsArray(imageIn), imageInDimension);
        int greyTRGB[][][] = pixelsArrayToTRGBArray(imageToPixelsArray(colorToGrayscale(imageIn)), imageInDimension);
        double threshold = iterThresholdSelect(extractHistogram(imageIn));
        
        for (int row = 0; row < imageInDimension.getHeight(); row++)
            for (int column = 0; column < imageInDimension.getWidth(); column++) {
                // black 0 or white 255
                if (greyTRGB[1][column][row] < threshold){
                    greyTRGB[1][column][row] = 0;
                    greyTRGB[2][column][row] = 0;
                    greyTRGB[3][column][row] = 0;
                } else {
                    greyTRGB[1][column][row] = 255;
                    greyTRGB[2][column][row] = 255;
                    greyTRGB[3][column][row] = 255;
                }
            }        
        return pixelsArrayToImage(TRGBArrayToPixelsArray(greyTRGB, imageInDimension), imageInDimension);
    }
    
    protected double imageVariance(int[] histogram, double low, double high){
        double result = 0;
        int count = 0;
        double temp;
        double overallMean = meanGrey(histogram, 0, 255);
        
        // log(bottom + " = bottom : " + top + " = top\n");
        for (int i = (int) low; i <= high; i++){
            temp = (i * histogram[i]) - overallMean;
            result += (temp * temp);
            count += histogram[i];
        }
        
        return result/count;
    }
    
    protected double betweenClassVariance(int[] histogram, double threshold, double totalPixels){
        // determines variance between classes.
        // I have to apply for jobs. This has to be priority.
        // w0 probability of pixels in low class
        // w1 probability of pixels in high class
        
        double w1, u0, u1, w0 = 0, ut = 0; 
        double overallMean = meanGrey(histogram, 0, 255);
        
        for (int i = 0; i < histogram.length; i++) {
            totalPixels += histogram[i];
        }
        
        log(threshold + " = threshold value reported in betweenClassVariance\n");
        for (int i = 0; i <= threshold; i++) {
            w0 += histogram[i] / totalPixels;
            ut += histogram[i] / totalPixels * i;
        }
        w1 = 1 - w0;
        
        log(w0 + " = w0; sum of probability below threshold\n");
        log(w1 + " = w1; sum of probability above threshold\n");
        u0 = ut / w0;
        u1 = (overallMean - ut) / w1;
        log(u0 + " = u0, or ut / w0;\n");
        log(u1 + " = u1, or (overallMean - ut) / w1;\n");
        
        // between class variance is w0*w1(u0 * u1)squared
        log("between class variance is: " + w0 * w1 * (u0 * u0 * u1 * u1) + "\n");
                
        return w0 * w1 * (u0 * u0 * u1 * u1);
    }
    
    protected Image bigDifference (Image imageIn, Image imageInAlt, int diffFactor){
        // declare storage
        Dimension imageInDimension = getImageDimension(imageIn);
        int TRGB[][][] = pixelsArrayToTRGBArray(imageToPixelsArray(imageIn), imageInDimension);
        // assumption of same dimensions Dimension imageInAltDimension = getImageDimension(imageInAlt);
        int altTRGB[][][] = pixelsArrayToTRGBArray(imageToPixelsArray(imageInAlt), imageInDimension);
        int countZero = 0;
        int countLow = 0;
        int countHigh = 0;
        int breakPoint = 4;
        int imageDiff;

        // Loop through image, find difference and alter altImage
        for (int row = 0; row < imageInDimension.getHeight(); row++)
            for (int column = 0; column < imageInDimension.getWidth(); column++)
                for (int color = 1; color < 4; color ++) {
                    
                    // multiply difference of images by diffFactor and altTRGB array
                    imageDiff = TRGB[color][column][row] - altTRGB[color][column][row];
                    if (imageDiff == 0)
                        countZero++;
                    if (Math.abs(imageDiff) <= breakPoint){
                        countHigh++;
                    } else {
                        countLow++;
                    }
                    
                    altTRGB[color][column][row] = altTRGB[color][column][row] + 
                            (imageDiff * diffFactor);
                    
                    // don't let the color rollover
                    if (altTRGB[color][column][row] < 0)
                        altTRGB[color][column][row] = 0;
                    if (altTRGB[color][column][row] > 255) 
                        altTRGB[color][column][row] = 255;
                }
        
        int total = countZero + countHigh + countLow;
        log ("\nTotal pixels:" + total + " countZero:" + countZero+ " countLow:" + countLow + " countHigh:" + countHigh );
        
        return pixelsArrayToImage(TRGBArrayToPixelsArray(altTRGB, imageInDimension), imageInDimension);
    }
    protected Image avgDifference (Image imageIn, Image imageInAlt, int diffFactor){
        // declare storage
        int[] x3 = {3,3};
        imageIn = averageImage(imageIn, x3);
        imageInAlt = averageImage(imageInAlt, x3);
        
        Dimension imageInDimension = getImageDimension(imageIn);
        int TRGB[][][] = pixelsArrayToTRGBArray(imageToPixelsArray(imageIn), imageInDimension);
        // assumption of same dimensions Dimension imageInAltDimension = getImageDimension(imageInAlt);
        int altTRGB[][][] = pixelsArrayToTRGBArray(imageToPixelsArray(imageInAlt), imageInDimension);
        int countZero = 0;
        int countLow = 0;
        int countHigh = 0;
        int breakPoint = 4;
        int imageDiff;

        // Loop through image, find difference and alter altImage
        for (int row = 0; row < imageInDimension.getHeight(); row++)
            for (int column = 0; column < imageInDimension.getWidth(); column++)
                for (int color = 1; color < 4; color ++) {
                    
                    // multiply difference of images by diffFactor and altTRGB array
                    imageDiff = TRGB[color][column][row] - altTRGB[color][column][row];
                    if (imageDiff == 0)
                        countZero++;
                    if (Math.abs(imageDiff) <= breakPoint){
                        countHigh++;
                    } else {
                        countLow++;
                    }
                    
                    altTRGB[color][column][row] = altTRGB[color][column][row] + 
                            (imageDiff * diffFactor);
                    
                    // don't let the color rollover
                    if (altTRGB[color][column][row] < 0)
                        altTRGB[color][column][row] = 0;
                    if (altTRGB[color][column][row] > 255) 
                        altTRGB[color][column][row] = 255;
                }
        
        int total = countZero + countHigh + countLow;
        log ("\nTotal pixels:" + total + " countZero:" + countZero+ " countLow:" + countLow + " countHigh:" + countHigh );
        
        return pixelsArrayToImage(TRGBArrayToPixelsArray(altTRGB, imageInDimension), imageInDimension);
    }
    
    protected Image optimalThreshold (Image imageIn){
        // declare storage
        Dimension imageInDimension = getImageDimension(imageIn);
        int greyTRGB[][][] = pixelsArrayToTRGBArray(imageToPixelsArray(colorToGrayscale(imageIn)), imageInDimension);// lowMean
        int[] histogram = extractHistogram(imageIn); 
        double overallVariance = imageVariance(histogram, 0, 255);
        log(overallVariance + " = overall variance\n");
        int pass = 1;
        
        double totalPixels = imageInDimension.getHeight() * imageInDimension.getWidth();
        // double initialIterThreshold = iterThresholdSelect(histogram);
        double initialIterThreshold = 180;
        double iterThreshold = initialIterThreshold;
        double savedOptimalThreshold = iterThreshold;
        double eta = betweenClassVariance(histogram, iterThreshold, totalPixels) / overallVariance;
        log(iterThresholdSelect(histogram) + " = iterThresholdSelect(histogram)\n"
                +  totalPixels + " = total pixels\n");
        log ("intial eta = " + eta + " = ");
        double etaNow;
        
        while (true) {
            
            iterThreshold = initialIterThreshold + pass;
            // if the iterThreshold gets too big or too small, stop
            if (iterThreshold < 0 || iterThreshold > 255){
                log(iterThreshold + " = iterThreshold, too big or too small\n");
                log(pass + " = pass\n");
                break;
            }
            
            etaNow = betweenClassVariance(histogram, iterThreshold, totalPixels)  / overallVariance;
            log(etaNow + " = etaNow\n");
            
            // if eta gets bigger, stop
            if (etaNow < eta){
                eta = etaNow; 
                savedOptimalThreshold = iterThreshold;
            } else {
                log ("new eta is higher\n");
                break;
            }
            
            // step one away from initial iterThreshold, search above and below.
            if (pass > 0){
                pass *= -1;
            } else {
                pass++;
            }
                
        }
        
        log (eta + " = eta final\n");
        log (savedOptimalThreshold + " = savedOptimalThreshold final\n");
        
        // int TRGB[][][] = pixelsArrayToTRGBArray(imageToPixelsArray(imageIn), imageInDimension);
        
        for (int row = 0; row < imageInDimension.getHeight(); row++)
            for (int column = 0; column < imageInDimension.getWidth(); column++) {
                // black or white
                if (greyTRGB[1][column][row] < savedOptimalThreshold){
                    greyTRGB[1][column][row] = 0;
                    greyTRGB[2][column][row] = 0;
                    greyTRGB[3][column][row] = 0;
                } else {
                    greyTRGB[1][column][row] = 255;
                    greyTRGB[2][column][row] = 255;
                    greyTRGB[3][column][row] = 255;
                }
            }
        
        return pixelsArrayToImage(TRGBArrayToPixelsArray(greyTRGB, imageInDimension), imageInDimension);
    }
    
    protected double compareNoise(Image imageIn){
        // When the pixels are represented using 8 bits per sample, this is 255.
        // More generally, when samples are represented using linear PCM with B
        // bits per sample, MAXI is 2B−1. For color images with three RGB values
        // per pixel, the definition of PSNR is the same except the MSE is the 
        // sum over all squared value differences divided by image size and by three. 
        
        // declare storage
        Dimension imageInDimension = getImageDimension(imageIn);
        int TRGB[][][] = pixelsArrayToTRGBArray(imageToPixelsArray(imageIn), imageInDimension);
        
        int winSize[] = {3,3};
        
        // hard coded test for median filter
        Image fixedImageIn = averageImage(addNoise(imageIn), winSize);
        int altTRGB[][][] = pixelsArrayToTRGBArray(imageToPixelsArray(fixedImageIn), imageInDimension);
        
        double valueDifference = 0;
        double pixelDifference;
        double mse;
        double psnr; 
        int r = 255;
        

        for (int row = 0; row < imageInDimension.getHeight(); row++){
            for (int column = 0; column < imageInDimension.getWidth(); column++) {
                pixelDifference = 0;
                for (int color = 1; color <= 3 ; color ++){
                    pixelDifference += (TRGB[color][column][row] - altTRGB[color][column][row]) 
                            * (TRGB[color][column][row] - altTRGB[color][column][row]);
                }
                valueDifference += (pixelDifference);
            }
        }
        // divide by 3, one for each 
        // valueDifference = valueDifference / 3.0;
        
        log("valueDifference: " + valueDifference + "\n");
        mse = valueDifference / (imageInDimension.getHeight() * imageInDimension.getWidth() * 3.0);
        log("mse: " + mse + "\n");
        log("r = " + r + "\n");
        psnr = 10 * Math.log10(r * r / (mse + 0.00000001));
        log("psnr: " + psnr + "\n");
        return psnr;
    }
    
    protected Dimension getImageDimension(Image imageIn) {
        // Convert image to an Image Icon to get the dimensions
        ImageIcon imageIconIn = new ImageIcon(imageIn);
        //System.out.println("width: "+ imageIconIn.getIconWidth()+" height: "+ imageIconIn.getIconHeight());
        return new Dimension(imageIconIn.getIconWidth(), imageIconIn.getIconHeight());
    }

    protected Image pixelsArrayToImage(int pixels[], Dimension imageInDimension) {
        // Generate an image object from the pixels array
        return createImage(new MemoryImageSource((int) imageInDimension.getWidth(), (int) imageInDimension.getHeight(), pixels, 0, (int) imageInDimension.getWidth()));
    }

    protected int[] imageToPixelsArray(Image imageIn) {
        // Declare local storage
        Dimension imageInDimension = getImageDimension(imageIn);
        int imagePixelLength = (int) (imageInDimension.getWidth() * imageInDimension.getHeight());
        int pixels[] = new int[imagePixelLength];

        // Convert image to array via PixelGrabber
        PixelGrabber pixelGrabber = new PixelGrabber(imageIn, 0, 0, (int) imageInDimension.getWidth(), 
                                                                    (int) imageInDimension.getHeight(), pixels, 0,
                                                                    (int) imageInDimension.getWidth());
        try {
            pixelGrabber.grabPixels();
        } catch(InterruptedException ie) {
            System.exit(1);
        }

        // Return the pixels array
        return pixels;
    }

    protected int[] TRGBArrayToPixelsArray(int[][][] TRGB, Dimension imageInDimension) {
        // Declare local storage
        int imagePixelLength = (int) (imageInDimension.getWidth() * imageInDimension.getHeight());
        int pixels[] = new int[imagePixelLength];

        // Loop through TRGB array, assembling the TRGB values
        for (int column = 0, row = 0, pixelIndex = 0; pixelIndex < imagePixelLength; pixelIndex++) {
            // Retreive TRGB
            pixels[pixelIndex] = getTRGB(TRGB[0][column][row], TRGB[1][column][row], TRGB[2][column][row], TRGB[3][column][row]);

            // Calculate column and row indexes
            if (++column == imageInDimension.getWidth()) {
                // Reset column and increment row
                column = 0;
                row++;
            }
        }
        // Return the pixels array
        return pixels;
    }

    protected int[][][] pixelsArrayToTRGBArray(int[] pixels, Dimension imageInDimension) {
        // Declare local storage
        int imagePixelLength = (int) (imageInDimension.getWidth() * imageInDimension.getHeight());
        int TRGB[][][] = new int[4][(int) imageInDimension.getWidth()][(int) imageInDimension.getHeight()];

        // Convert pixel array to TRGB array
        for (int column = 0, row = 0, pixelIndex = 0; pixelIndex < imagePixelLength; pixelIndex++) {
            // Store transparency
            TRGB[0][column][row] = getTransparencyComponent(pixels[pixelIndex]);

            // Store red
            TRGB[1][column][row] = getRedComponent(pixels[pixelIndex]);

            // Store green
            TRGB[2][column][row] = getGreenComponent(pixels[pixelIndex]);

            // Store blue
            TRGB[3][column][row] = getBlueComponent(pixels[pixelIndex]);

            // Calculate column and row indexes
            if (++column == imageInDimension.getWidth()) {
                // Reset column and increment row
                column = 0;
                row++;
            }
        }
        // Return the newly generated TRGB array
        return TRGB;
    }

    // TRGB Component retrieval functions //////////////////////////////
    protected final int getTransparencyComponent(int pixel) {
        return (pixel >> 24) & 0xff; // Return Transparency
    }

    protected final int getRedComponent(int pixel) {
        return (pixel >> 16) & 0xff; // Return Red
    }

    protected final int getGreenComponent(int pixel) {
        return (pixel >> 8) & 0xff; // Return Green
    }

    protected final int getBlueComponent(int pixel) {
        return pixel & 0xff; // Return Blue
    }

    protected final int getTRGB(int transparency, int red, int green, int blue) {
        return (transparency << 24) | (red << 16) | (green << 8) | (blue); // Return TRGB
    }

    // Public

    // Inner Class(es) /////////////////////////////////////////////////
    // Private

    // Protected

    // Public
}

class ImageFrame extends JInternalFrame {
    // Instance Variable(s) ////////////////////////////////////////////
    // Constant(s)

    // Data Member(s)
    // Private
    private File imageFile;
    private ImagePanel imagePanel;

    // Protected

    // Public

    // Constructor(s) //////////////////////////////////////////////////
    public ImageFrame(File imageFile) {
        // Call super class, JInternalFrame constructor
        super(imageFile.getName(), false, true, false);

        // Attempt to load image
        imagePanel = new ImagePanel(imageFile);
        getContentPane().add(imagePanel);

        // Set the internal image file to the passed argument
        this.imageFile = imageFile;
        this.setTitle(imageFile.getName());
        // Initialize and show the internal frame window
        pack();
        int verticalAdd = this.getInsets().top + this.getInsets().bottom;
        int horizontalAdd = this.getInsets().top + this.getInsets().bottom;
        getContentPane();
        //System.out.print(verticalAdd + " vertical:" + horizontalAdd + " horizontal, file, pack\n"
        //		+ ImageObserver.WIDTH + " this.WIDTH:" + ImageObserver.HEIGHT + " this.HEIGHT");
        // still can't identify height of title bar.
        // when solved, add to ImageFrame method below
        // http://stackoverflow.com/questions/12803963/how-can-i-get-the-height-of-the-title-bar-of-a-jinternalframe
        setSize(imagePanel.getImageIcon().getIconWidth() + horizontalAdd + 1, imagePanel.getImageIcon().getIconHeight() + verticalAdd + 24);
        show();
        toFront();
        this.getContentPane();
        //System.out.print("\nafter\n" + verticalAdd + " vertical:" + horizontalAdd + " horizontal, file, pack\n"
        //		+ ImageObserver.WIDTH + " this.WIDTH:" + ImageObserver.HEIGHT + " this.HEIGHT");
    }

    public ImageFrame(Image image) {
        // Call super class, JInternalFrame constructor
        super("Untitled", false, true, false);

        // Attempt to load image
        imagePanel = new ImagePanel(image);
        getContentPane().add(imagePanel);

        // Set the internal image file to a default name
        imageFile = null;

        // Initialize and show the internal frame window
        int verticalAdd = this.getInsets().top + this.getInsets().bottom;
        int horizontalAdd = this.getInsets().top + this.getInsets().bottom;
        // System.out.print(verticalAdd + " vertical:" + horizontalAdd + " horizontal, image, nopack");
        // adding pixels to display entire image. insets and title bar are not automatically included
        setSize(imagePanel.getImageIcon().getIconWidth() + 11, imagePanel.getImageIcon().getIconHeight() + 34);
        //setSize(imagePanel.getImageIcon().getIconWidth(), imagePanel.getImageIcon().getIconHeight()); // original
        show();
        toFront();
    }

    // Finalize ////////////////////////////////////////////////////////

    // Method(s) ///////////////////////////////////////////////////////
    // Private

    // Protected

    // Public
    public Image getImage() {
        // Return the image icon from the image panel, after conversion to an image
        return imagePanel.getImageIcon().getImage();
    }
    
    public boolean saveImage(File imageFile) {
        //	try
        //	{
        //		FileOutputStream os = new FileOutputStream(imageFile);
        //		JAI.create("encode",imagePanel.getImageIcon().getImage(),os,BMP,null);
        //		JAI.create("filestore",imagePanel.getImageIcon().getImage(),imageFile.getName(),BMP,null);
        //		os.close();
        //	}catch(Exception e){return false;}

        return true;
    }

    // Inner Class(es) /////////////////////////////////////////////////
    // Private

    private class ImagePanel extends JPanel {
        // Instance Variable(s) ////////////////////////////////////////
        // Constant(s)

        // Data Member(s)
        // Private
        private final ImageIcon imageIcon;

        // Protected

        // Public

        // Constructor(s) //////////////////////////////////////////////
        public ImagePanel(File imageFile) {
            // Load the image
            imageIcon = new ImageIcon(imageFile.toString());
        }

        public ImagePanel(Image image) {
            // Load the image
            imageIcon = new ImageIcon(image);
        }

        // Finalize ////////////////////////////////////////////////////

        // Method(s) ///////////////////////////////////////////////////
        // Private

        // Protected

        // Public
        public ImageIcon getImageIcon() {
            // Return the image icon
            return imageIcon;
        }

        public void paintComponent(Graphics g) {
            // Paint the icon to the panel
            imageIcon.paintIcon(this, g, 0, 0);
        }

        // Inner Class(es) ////////////////////////////////////////////
        // Private

        // Protected

        // Public

        // Protected

        // Public
    }
}